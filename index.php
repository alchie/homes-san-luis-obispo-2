<?php
get_header(); ?>
<div class="sixteen columns">
		<div id="content" class="single">

			<div id="main" class="twelve columns" role="main">
				<?php while ( have_posts() ) : the_post(); ?>

					<nav id="nav-single">
						<h3 class="assistive-text"><?php _e( 'Post navigation', 'homessanluisobispo' ); ?></h3>
						<span class="nav-previous"><?php previous_post_link( '%link', __( '<span class="meta-nav">&larr;</span> Previous', 'homessanluisobispo' ) ); ?></span>
						<span class="nav-next"><?php next_post_link( '%link', __( 'Next <span class="meta-nav">&rarr;</span>', 'homessanluisobispo' ) ); ?></span>
					</nav><!-- #nav-single -->

					<?php get_template_part( 'content', get_post_format() ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>
			</div>
			<?php get_sidebar(); ?>
			<div class="clear"></div>
		</div>	
</div>

<?php get_footer(); ?>