<?php 
get_header(); ?>
		<div class="sixteen columns">
		<div id="content" class="homepage">
			<div id="sidebar" class="four columns">
				<div id="features">
				<h3>Featuring:</h3>
				<ul>
					<li>Oceanfront Homes</li>
					<li>Oceanview Homes</li>
					<li>Estate on Acreage</li>
					<li>Equestrian Properties</li>
					<li>Homes with Vineyards</li>
					<li>Homes with Orchards</li>
					<li>Green Homes</li>
					<li>Golf Course Homes</li>
					<li>Smart Homes</li>
					<li><a href="#" class="videotour">Video Tours</a></li>
				</ul>
				
				</div>
			</div>
			<div id="main" class="twelve columns nivoslider">
				<img src="<?php echo get_template_directory_uri(); ?>/images/image1.jpg" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/image2.jpg" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/image3.jpg" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/image4.jpg" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/image5.jpg" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/image6.jpg" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/image7.jpg" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/image8.jpg" />
			</div>
			<div class="clear"></div>
		</div>	
		</div>
		<div id="footer-boxes">
			<div class="one-third column">
				<div id="searchprop" class="box">
					<h3><a href="#">Search Properties</a></h3>
					<span>on the Central Coast of California</span>
				</div>
			</div>
			<div class="one-third column">
				<div class="box">
					<h3><a href="#">Tell us what you want</a></h3>
					<span>in your next home purchase</span>
				</div>
			</div>
			<div class="one-third column">
				<div class="box">
					<h3><a href="#">luxury home guide</a></h3>
					<span>of homes for sale $1,500,000 and above</span>
				</div>
			</div>
		</div>
<?php get_footer(); ?>