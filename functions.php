<?php
add_action( 'after_setup_theme', 'homessanluisobispo_setup' );

function homessanluisobispo_setup() {
	register_nav_menu( 'primary', __( 'Primary Menu', 'homessanluisobispo' ) );

	wp_register_script( 'jQuery1.7.1', get_bloginfo('template_directory') . "/js/jquery-1.7.1.min.js", null, '1.7.1', true);
	wp_register_script( 'nivoSlider', get_bloginfo('template_directory') . "/js/nivoSlider/jquery.nivo.slider.pack.js", array('jQuery1.7.1'), '3.1', true);
	wp_register_style( 'nivoSlider', get_bloginfo('template_directory') . "/js/nivoSlider/nivo-slider.css" );
}

add_action('wp_enqueue_scripts', 'homessanluisobispo_nivoslider'); 
function homessanluisobispo_nivoslider() {
	wp_enqueue_script( 'nivoSlider' );
	wp_enqueue_style( 'nivoSlider' );
}
add_action('wp_footer', 'homessanluisobispo_nivoslider_run', 100); 
function homessanluisobispo_nivoslider_run() {
	?>
<script type="text/javascript">
jQuery(window).load(function() {
	$('.nivoslider').nivoSlider({
		animSpeed: 1000, 
        pauseTime: 7000, 
		directionNav: false,
        controlNav: false, 
        pauseOnHover: false,
	});
});
</script>
	<?php
}

function homessanluisobispo_posted_on() {
	printf( __( '<span class="sep">Posted on </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a><span class="by-author"> <span class="sep"> by </span> <span class="author vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span>', 'twentyeleven' ),
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'View all posts by %s', 'twentyeleven' ), get_the_author() ) ),
		get_the_author()
	);
}