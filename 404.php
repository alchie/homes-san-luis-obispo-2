<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

	<div id="primary">
		<div id="no-sidebar-container">
			<div class="notice">
				<h2 class="title">Page Not Found!</h2>
<p>Please select the "back" button or use</p>
<p>the navigation to get you to the correct page.</p>
			</div>
		</div><!-- #content-container -->
	</div><!-- #primary -->
<?php get_footer(); ?>